package com.eztravel.checkout;

import java.util.ArrayList;
import java.util.List;

public class CheckoutController {

	CheckoutService checkoutService;
	
	public int checkoutLunchBox(int unit) {
		List<Fruit> fruits = new ArrayList<Fruit>();
		Fruit apple = new Fruit();
		apple.setName("Apple");
		apple.setUnit(1);
		fruits.add(apple);
		Fruit cherry = new Fruit();
		cherry.setName("Cherry");
		cherry.setUnit(2);
		fruits.add(cherry);
		return checkoutService.checkout(fruits);
	}
	
}
