package com.eztravel.checkout;

public class Fruit {

	String name;
	
	int unit;
	
	Boolean isOnsale = false;

	public Boolean isOnsale() {
		return isOnsale;
	}

	public void setOnsale(Boolean isOnsale) {
		this.isOnsale = isOnsale;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getUnit() {
		return unit;
	}

	public void setUnit(int unit) {
		this.unit = unit;
	}
	
}
